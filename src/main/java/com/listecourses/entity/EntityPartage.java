package com.listecourses.entity;

import javax.persistence.*;

@Entity
@Table(name = "partage_shoppinglist")
public class EntityPartage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long idShoppingList;
    private long idUserPartage;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdShoppingList() {
		return idShoppingList;
	}
	public void setIdShoppingList(long idShoppingList) {
		this.idShoppingList = idShoppingList;
	}
	public long getIdUserPartage() {
		return idUserPartage;
	}
	public void setIdUserPartage(long idUserPartage) {
		this.idUserPartage = idUserPartage;
	}
	public EntityPartage() {}
	
	public EntityPartage(long idShoppingList, long idUserPartage) {
		this.idShoppingList = idShoppingList;
		this.idUserPartage = idUserPartage;
	}
    
	
	
}
