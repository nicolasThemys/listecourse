package com.listecourses.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nickName;
    private String password;
    private Date creationDate;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
    private List<ShoppingList> shoppingLists;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idUserPartage")
    private List<EntityPartage> shoppingListpartage;
    
    public List<EntityPartage> getshoppingListPartage(){
    	return shoppingListpartage;
    }
    
    public List<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

    public void setShoppingLists(List<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
