package com.listecourses.beans;

public class LoginResponse {

	private long userid;
	private String nickname;
	
		
	
	public long getId() {
		return userid;
	}
	
	public String getnickname() {
		return nickname;
	}
	
	private LoginResponse(long userid, String nickname) {
		super();
		this.userid = userid;
		this.nickname = nickname;
	}
	
	public static LoginResponseBuilder builder() {
		return new LoginResponseBuilder();
	}
	
	public static class LoginResponseBuilder{
		private long userId;
		private String nickname;
		
		public LoginResponseBuilder withUserId(long userId) {
			this.userId = userId;
			return this;
		}
		
		public LoginResponseBuilder withNickname(String nickName) {
			this.nickname = nickName;
			return this;
		}
		
		public LoginResponse build() {
			return new LoginResponse(userId, nickname);
		}
	}
	
	
}
