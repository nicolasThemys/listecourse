package com.listecourses.beans;

public class PartageListRequest {
	
	private int id;
	private int id_ShoppingList;
	private int id_User_Partage;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_ShoppingList() {
		return id_ShoppingList;
	}
	public void setId_ShoppingList(int id_ShoppingList) {
		this.id_ShoppingList = id_ShoppingList;
	}
	public int getId_User_Partage() {
		return id_User_Partage;
	}
	public void setId_User_Partage(int id_User_Partage) {
		this.id_User_Partage = id_User_Partage;
	}

	
}
