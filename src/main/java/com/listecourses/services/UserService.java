package com.listecourses.services;

import com.listecourses.entity.EntityPartage;
import com.listecourses.entity.ShoppingList;
import com.listecourses.entity.User;
import com.listecourses.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;


   	public User getUserByNickNameAndPassword(String nickName, String password) {
		return repository.findByNickNameAndPassword(nickName, password);
	}

	public List<ShoppingList> getUserShoppingList(Long userId) {

		List<ShoppingList> shoppingList = new ArrayList<>();
		return repository.findOne(userId).getShoppingLists();
	}

	public void createUser(User user) {
		// TODO Auto-generated method stub
		
	}
	public List<EntityPartage> getUserShoppingListPartage(Long userId) {

		return repository.findOne(userId).getshoppingListPartage();
	}
	
}
