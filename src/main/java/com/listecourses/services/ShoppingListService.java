package com.listecourses.services;

import com.listecourses.entity.Category;
import com.listecourses.entity.ShoppingList;
import com.listecourses.repository.CategoryRepository;
import com.listecourses.repository.ProductRepository;
import com.listecourses.repository.ShoppingListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ShoppingListService {

    @Autowired
    private ShoppingListRepository repository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;

    public void createShoppingList(ShoppingList shoppingList) {
        shoppingList.setCreationDate(new Date());
        repository.save(shoppingList);
    }

    public void addCategoryToList(long listId, long categoryId) {

        // TODO 11 empêcher d'ajouter une catégorie qui est déjà dans la liste
        ShoppingList list = repository.findOne(listId);
        if(!list.getCategories().contains(categoryRepository.findOne(categoryId))) {
        	  	list.getCategories().add(categoryRepository.findOne(categoryId));
        	repository.save(list);
        }
    }

    public void deleteList(long id) {
        repository.delete(id);
    }
    
    public List<Category> getShoppingListCategory(Long id) {

		List<Category> shoppingListCategory = new ArrayList<>();

		return repository.findOne(id).getCategories();
	}
    
    public void deleteShoppingList(long id) {
        repository.delete(id);
    }
    
    public void deleteCategoryFromshoppingList(long shoppingListId, long categoryId) {
    	ShoppingList spl = repository.findOne(shoppingListId);
    	spl.getCategories().remove(categoryRepository.findOne(categoryId));
    	productRepository.delete(productRepository.findByListIdAndCategoryId(categoryId, shoppingListId));
    	repository.save(spl);
    }
    
  //TODO 9 COMMENT SUPPRIMER LES PRODUITS D'UNE CATEGORIES
    
   
}
