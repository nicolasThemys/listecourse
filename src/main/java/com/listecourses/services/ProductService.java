package com.listecourses.services;

import com.listecourses.entity.Product;
import com.listecourses.entity.ShoppingList;
import com.listecourses.repository.CategoryRepository;
import com.listecourses.repository.ProductRepository;
import com.listecourses.repository.ShoppingListRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private ShoppingListRepository reposhop;
	
	@Autowired
	private CategoryRepository repocat;
	
	
	public void createProduct(Product product) {
		
		 ShoppingList list = reposhop.findOne(product.getListId());
	        if(list.getCategories().contains(repocat.findOne(product.getCategoryId()))) {
	        	product.setCreationDate(new Date());
				repository.save(product);
	        	
	        }
	    }
		

	public void deleteproduct(long id) {
		repository.delete(id);
	}

	public List<Product> ListeProduit(long listId, long categoryId) {
		List<Product> products = repository.findByListIdAndCategoryId(listId, categoryId);
		return products;
		
	}  
}
