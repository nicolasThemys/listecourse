package com.listecourses.services;

import com.listecourses.entity.EntityPartage;
import com.listecourses.repository.PartageListRequestRepository;
import com.listecourses.repository.ShoppingListRepository;
import com.listecourses.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartageListRequestService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private ShoppingListRepository shr;
	
	@Autowired
	private PartageListRequestRepository PartageRepository;
		
	public void UserShoppinglistPartage(long idList, String nickName) {
		long Useridpartage = repository.findByNickName(nickName).getId();
		
		if(shr.findOne(idList).getOwner().getId() != repository.findByNickName(nickName).getId()) {
			EntityPartage T = new EntityPartage(idList, Useridpartage);
			PartageRepository.save(T);	
		}
	}
	

}
