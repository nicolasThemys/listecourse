package com.listecourses.repository;

import org.springframework.data.repository.CrudRepository;

import com.listecourses.entity.EntityPartage;

public interface PartageListRequestRepository extends CrudRepository<EntityPartage, Long> {
	
}
