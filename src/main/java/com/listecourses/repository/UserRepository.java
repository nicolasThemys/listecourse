package com.listecourses.repository;

import com.listecourses.entity.User;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {

    User findByNickNameAndPassword(String nickName, String password);
    User findByNickName(String nickName);
   // boolean existsByNickName(String nickName);
}
