package com.listecourses.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.listecourses.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
	
	List<Product> findByListIdAndCategoryId(long listId, long categoryId);
}
