package com.listecourses.security;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SessionToken {

    private Map<Long, String> usersToken = new HashMap<>();

    public Map<Long, String> getTokens() {
        return usersToken;
    }

    public boolean isTokenValid(String token) {
        return usersToken.containsValue(token);
    }

    public void addToken(long userId, String token) {
        if(!usersToken.containsKey(userId)) {
            usersToken.put(userId, token);
        } else {
            // Todo
        }
    }

    public void deleteToken(long userId) {
        usersToken.remove(userId);
    }
}
