package com.listecourses.security;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SecuredMethodsAspect {

    @Autowired
    private SessionToken sessionToken;

    @Around("@annotation(com.listecourses.security.Secure)")
    public void runBeforeSecuredMethods(ProceedingJoinPoint joinPoint) throws Throwable {

        // TODO 500000+ remettre en place le système de sécurité
        if(sessionToken.isTokenValid(joinPoint.getArgs()[1].toString())) {
            joinPoint.proceed();
        } else {
            throw new RuntimeException();
        }

    }
}
