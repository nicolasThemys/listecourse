package com.listecourses.controllers;


import com.listecourses.services.PartageListRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/userspartage")
public class PartageListREquestController {

	@Autowired
	private PartageListRequestService plrs;

	 
    // TODO 13 mettre en place toute la gestion des listes partagées
    @RequestMapping(path="{idList}/{PseudoPartage}/partageList", method = RequestMethod.POST)
	public void UserShoppinglistPartage( @PathVariable("idList") long idList,  @PathVariable("PseudoPartage") String nickName) {
    	plrs.UserShoppinglistPartage(idList, nickName);
    	    	
	}
   
    
}
