package com.listecourses.controllers;

import com.listecourses.beans.AddCategoryRequest;
import com.listecourses.beans.CreateShoppingListRequest;
import com.listecourses.entity.Category;
import com.listecourses.entity.ShoppingList;
import com.listecourses.entity.User;
import com.listecourses.security.Secure;
import com.listecourses.services.ShoppingListService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@CrossOrigin
@RestController
@RequestMapping("/shoppinglist")
public class ShoppingListController {

    @Autowired
    private ShoppingListService service;

    @Secure
    @RequestMapping(method = RequestMethod.POST)
    public void createShoppingList(@RequestBody CreateShoppingListRequest createShoppingListRequest,
                                   @RequestHeader(value="token") String token) {

        ShoppingList sl = new ShoppingList();
        sl.setName(createShoppingListRequest.getName());
        User owner = new User();
        owner.setId(createShoppingListRequest.getUserId());
        sl.setOwner(owner);
        service.createShoppingList(sl);

    }

    @Secure
    @RequestMapping(path="/addcategory", method = RequestMethod.POST)
    public void addCategoryToShoppingList(@RequestBody AddCategoryRequest addCategoryRequest,
                                          @RequestHeader(value="token") String token) {
        service.addCategoryToList(addCategoryRequest.getListId(), addCategoryRequest.getCategoryId());
    }

    // TODO 3 get categories of a shopping list
    @RequestMapping(path = "/{id}/listecategory",method = RequestMethod.GET)
    public List<Category> getShoppingListCategory(@PathVariable("id") long id)  {
        return service.getShoppingListCategory(id);
    }

    // TODO 5 delete a shopping list
    @Secure
	@RequestMapping(path="{id}/deleteListe", method = RequestMethod.GET)
	public void deleteShoppingList( @PathVariable("id") long id, @RequestHeader(value = "token") String token) {
		service.deleteList(id);
	}

    // TODO 6 delete category of a shopping list
    @RequestMapping(path = "/{shoppinglistid}/deletecategory/{categoryid}",method = RequestMethod.DELETE)
    public void deleteCategoryofShoppingList( @PathVariable("shoppinglistid") long shoppingListId, 
    		@PathVariable("categoryid") long categoryId, 
    		@RequestHeader(value = "token") String token) {
    		service.deleteCategoryFromshoppingList(shoppingListId, categoryId);
    	}
    
    
}
