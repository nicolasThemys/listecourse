package com.listecourses.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.listecourses.entity.Product;
import com.listecourses.entity.ShoppingList;
import com.listecourses.repository.CategoryRepository;
import com.listecourses.security.Secure;
import com.listecourses.services.ProductService;

// TODO mettre les bonnes annotations
@CrossOrigin
@RestController
@RequestMapping("/produits")
public class ProductController {

	@Autowired
	private ProductService sl;

	// TODO 4 add product to list
	@Secure
	@RequestMapping(method = RequestMethod.POST)
	public void createProduct(@RequestBody Product product, @RequestHeader(value = "token") String token) {

		sl.createProduct(product);
	}

	// TODO 7 delete product
	@Secure
	@RequestMapping(path="{id}/deleteproduit", method = RequestMethod.GET)
	public void deleteproduct( @PathVariable("id") long id, @RequestHeader(value = "token") String token) {
		sl.deleteproduct(id);
	}

	// TODO 8 get products of a Category
	
	@RequestMapping(path="{listId}/{categoryId}/listeProduit", method = RequestMethod.GET)
	public List<Product> ListeProduit( @PathVariable("listId") long listId,  @PathVariable("categoryId") long categoryId) {
		List<Product> produits = sl.ListeProduit(listId, categoryId);
		return produits;
	}
	 
    // TODO 12 empêcher l'ajout d'un produit dans une catégorie qui n'est pas dans la shopping list

}
