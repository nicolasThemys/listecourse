package com.listecourses.controllers;


import com.listecourses.beans.CreateUserRequest;
import com.listecourses.beans.LoginResponse;
import com.listecourses.entity.Category;
import com.listecourses.entity.EntityPartage;
import com.listecourses.entity.Product;
import com.listecourses.entity.ShoppingList;
import com.listecourses.entity.User;
import com.listecourses.security.SessionToken;
import com.listecourses.security.TokenGenerator;
import com.listecourses.services.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService service;

    @Autowired
    private SessionToken sessionToken;
    

    // TODO 1 ici changer le type de retour pour ne remonter que l'id de l'utilisateur et son nickname
    // How TODO Faire une mapper et appliquer le design pattern builder

    @RequestMapping(path = "login", method = RequestMethod.POST)
    	public LoginResponse LoginReponse(@RequestBody User user) {
        User userFound = service.getUserByNickNameAndPassword(user.getNickName(), user.getPassword());
        sessionToken.addToken(user.getId(), TokenGenerator.generateToken());
        //return userFound;
        return LoginResponse.builder().withUserId(userFound.getId()).withNickname(userFound.getNickName()).build();
    	}
    
    
    @RequestMapping(method = RequestMethod.POST)
    public void createUser(@RequestBody CreateUserRequest request) {

        User user = new User();
        user.setNickName(request.getNickName());
        user.setPassword(request.getPassword());

        service.createUser(user);
    }
    
   
    // TODO 2 get user's shopping list

    @CrossOrigin
    @RequestMapping(path = "/{id}/liste",method = RequestMethod.GET)
    public List<ShoppingList> getUserShoppingList(@PathVariable("id") long userId)  {
        return service.getUserShoppingList(userId);
    }
    
    @CrossOrigin
    @RequestMapping(path = "/{id}/listepartage",method = RequestMethod.GET)
    public List<EntityPartage> getUserShoppingListPartage(@PathVariable("id") long userId)  {
        return service.getUserShoppingListPartage(userId);
    }


    // TODO 13 mettre en place toute la gestion des listes partagées
  
}
